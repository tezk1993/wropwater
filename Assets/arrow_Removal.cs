﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class arrow_Removal : MonoBehaviour
{
    public ProgressManager progressManager;
    public bool isVisited = false;
    public List<int> achievementIndexs;
    private int counter;


    private void Awake()
    {
        progressManager = GameObject.FindGameObjectWithTag("ProgressManager").GetComponent<ProgressManager>();
        progressManager.arrowScripts.Add(this);
    }

    public void removeArrows()
    {
        counter = 0;
        for (int i = 0; i < achievementIndexs.Count; i++)
        {
            if (progressManager.achievementDoneCheck(achievementIndexs[i]))
            {
                counter++;
            }

            if (counter == achievementIndexs.Count)
            {
                gameObject.SetActive(false);
            }
        }
    }
 


}
