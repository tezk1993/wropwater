﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
        Vector3 x;
        public float rotationSpeed = 10f;
        public float bobbingSpeed = 1f;
        public float shiftTime;
        // Update is called once per frame
        void Update()
        {
            shiftTime += Time.deltaTime;
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
            if (shiftTime < 1.0f)
            {
                x = transform.position;
                x.y += bobbingSpeed * Time.deltaTime;
                transform.position = x;
            }
            if (shiftTime > 1.0f)
            {
                x = transform.position;
                x.y -= bobbingSpeed * Time.deltaTime;
                transform.position = x;
            }
            if (shiftTime > 2.0f)
            {
                shiftTime = 0.0f;
            }
        }
    
}
