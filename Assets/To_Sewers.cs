﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class To_Sewers : MonoBehaviour
{


    public GameObject sewerEntrance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")){ 
            other.gameObject.transform.position = sewerEntrance.gameObject.transform.position;
            other.gameObject.GetComponent<simpleMovement>().RainParticles.SetActive(false);
            SoundManager.instance.ChangeMusicSource(other.gameObject.GetComponent<simpleMovement>().Sewer1);
         }
    }
}
