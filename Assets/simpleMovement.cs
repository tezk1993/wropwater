﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Homebrew;
public class simpleMovement : MonoBehaviour
{
    [Foldout("Attributes", true)]
    public float speed;
    public float currJump;
    public bool isJumping;
    public bool isFalling;
    public bool isGrounded;

    public float jumpSpeed;
    public float allowedJump;
    [SerializeField] private float raycastLength;       // Reference to the transform containing the camera.
    public bool primaryTrigger;

    public bool resetTrigger;

    public bool hasHitGround;
    public float m_damping = 0.5f;
    public Vector3 startPos;


    [Foldout("References", true)]
    public OVRCameraRig camRig;
    public GameObject flyingCamera;
    public GameObject groundCamera;
    public GameObject waterDrop;
    public ProgressManager progressManager;
    public GameObject RainParticles;
    public GameObject MenuCanvas;
    public GameObject AchCanvas;
    [Foldout("Audio", true)]

    public AudioClip Rain1;
    public AudioClip Sewer1;
    public AudioClip Splash1;
    public AudioClip Jump1;
    public AudioClip Wind1;


    private const float k_ExpDampingCoef = -5f;                // The coefficient used to damp the movement of the flyer.
    private const float k_BankingCoef = 0f;                     // How much the ship banks when it moves.
    private bool musicChanged;
    private Vector3 movementVector;
    private Rigidbody rb;

    int mask = 1 << 10;//LayerMask.GetMask("Environment")

   

    private void Awake()
    {  
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);


        rb = gameObject.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Environment") && !hasHitGround)
        {
            Debug.Log("Collided with environemnt");
            transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            SoundManager.instance.PlaySingle(Splash1);
            SoundManager.instance.ChangeMusicSource(Rain1);

            flyingCamera.SetActive(false);
            groundCamera.SetActive(true);
            camRig = groundCamera.transform.GetChild(0).GetComponent<OVRCameraRig>();
            hasHitGround = true;
            isGrounded = true;
            rb.useGravity = true;
            progressManager.groundscreenFade.OnLevelFinishedLoading();
        }
    }

    void Update()
    {

        if (OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad) || Input.GetMouseButton(1) || OVRInput.GetDown(OVRInput.Button.One))
        {
            Debug.Log("Test");
            SceneManager.LoadScene(0);
            ResetPlayer(0);
        }

        Move();
    }


    public void Move()
    {
        if (hasHitGround == true)
        {
          
            isGrounded = (Physics.Raycast(gameObject.transform.position, Vector3.down, raycastLength, mask)); // raycast down to look for ground is not detecting ground? only works if allowing jump when grounded = false; // return "Ground" layer as layer
            if (isGrounded == true)
            {
                isJumping = false;
            }
            primaryTrigger = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
            //Debug.Log("Camera forward" + camRig.centerEyeAnchor.transform.forward);
            //Debug.Log("Camera forward + transfor.pos " + transform.position + camRig.centerEyeAnchor.transform.forward);
            Vector3 rotationVector = new Vector3(0, camRig.centerEyeAnchor.rotation.eulerAngles.y, 0f);
            if (!progressManager.showingAchievement)
            {
                if(MenuCanvas.activeSelf == false && AchCanvas.activeSelf == false)
                {
                    gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, Quaternion.Euler(rotationVector), m_damping * (1f - Mathf.Exp(k_ExpDampingCoef * Time.deltaTime)));
                }
                
                movementVector = camRig.centerEyeAnchor.forward;
                if (isGrounded && movementVector.y < 0.2f && primaryTrigger == true || isGrounded && movementVector.y < 0.2f && Input.GetMouseButton(0))
                {
                    if (hasHitGround)
                    {
                        movementVector.y = 0;
                    }
                    //transform.position = transform.position + movementVector * speed * Time.deltaTime;
                    rb.velocity = new Vector3(movementVector.x * speed, movementVector.y * speed, movementVector.z * speed);

                }
                else if (hasHitGround && movementVector.y > 0.2f && Input.GetMouseButton(0) && isGrounded == true && isJumping == false)
                {
                    movementVector.y = 0;
                    rb.velocity = new Vector3(movementVector.x * speed, movementVector.y * speed, movementVector.z * speed);
                    Jump();
                    Debug.Log("Jumping");

                }
            }
        }
        else
        {
            Vector3 rotationVector = new Vector3(0f, camRig.centerEyeAnchor.rotation.eulerAngles.y, 0f);
            if(MenuCanvas.activeSelf == false && AchCanvas.activeSelf == false)
            {
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, Quaternion.Euler(rotationVector),
                m_damping * (1f - Mathf.Exp(k_ExpDampingCoef * Time.deltaTime)));

            }
            
            movementVector = camRig.centerEyeAnchor.forward;
            rb.velocity = new Vector3(movementVector.x * speed * 3, movementVector.y * speed * 3, movementVector.z * speed * 3);
        }
    }
    public void Jump()
    {
        Debug.Log("Jump");
        
        movementVector.y = 3;
        if(isJumping == false)
        {
            SoundManager.instance.PlaySingle(Jump1);
            rb.AddForce(movementVector, ForceMode.Impulse);
        }
        isJumping = true;
        //isGrounded = false;
        /*
         if (currJump < allowedJump)
         {
             float temp = 0.0f;
             temp = Mathf.Sin(Time.deltaTime) * jumpSpeed;
             currJump += temp;
             gameObject.transform.Translate(Vector3.up * temp * Time.deltaTime * jumpSpeed);
         }
         else
         {
             isJumping = false;
             isFalling = true;
         }
         */
    }

    public void Fall()
    {
        if (isGrounded == false)
        {
            float temp = 0.0f;
            temp = Mathf.Sin(Time.deltaTime) * jumpSpeed;
            currJump -= temp;
            gameObject.transform.Translate(Vector3.up * temp * Time.deltaTime * jumpSpeed * -1);
            Debug.Log((Physics.Raycast(gameObject.transform.position, Vector3.down, 0.1f, mask)));

        }
        else
        {
            isFalling = false;
            currJump = 0;
        }
    }

    public void ResetPlayer(int index)
    {
        RainParticles.SetActive(true);
        if (index == 1)
        {
            SoundManager.instance.ChangeMusicSource(Wind1);
            progressManager.showingAchievement = false;
            transform.position = startPos;
            movementVector = Vector3.zero;
            flyingCamera.SetActive(true);
            groundCamera.SetActive(false);
            camRig = flyingCamera.transform.GetChild(0).GetComponent<OVRCameraRig>();
            hasHitGround = false;
            isGrounded = false;
            rb.useGravity = false;
            rb.isKinematic = false;

        }
        else
        {
            MenuCanvas.SetActive(true);
            SoundManager.instance.ChangeMusicSource(Rain1);
            progressManager.showingAchievement = true;
            transform.position = startPos;
            movementVector = Vector3.zero;
            flyingCamera.SetActive(false);
            groundCamera.SetActive(true);
            camRig = groundCamera.transform.GetChild(0).GetComponent<OVRCameraRig>();
            hasHitGround = true;
            isGrounded = false;
            rb.useGravity = false;
            rb.isKinematic = true;
        }
     
    }
}
