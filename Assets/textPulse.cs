﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textPulse : MonoBehaviour
{
    public UIFader uiFaderscript;

    // Start is called before the first frame update

    private void Awake()
    {

        //uiFaderscript.CheckAndFadeIn();
        StartCoroutine(uiFaderscript.CheckAndFadeOut());

        uiFaderscript.OnFadeOutComplete += fadeOutFunc;
        uiFaderscript.OnFadeInComplete += fadeInFunc;

    }

    void fadeOutFunc()
    {
        StartCoroutine(uiFaderscript.FadeIn());

        Debug.Log("Fadeout");
    }
    void fadeInFunc()
    {
        StartCoroutine(uiFaderscript.FadeOut());
        Debug.Log("Fadein");
    }
}
