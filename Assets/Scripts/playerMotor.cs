﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;

public class playerMotor : MonoBehaviour
{
    public OVRCameraRig ovrCamera;
    Vector3 headsetDir;    
    [Header("Options")]
    [SerializeField]
    public float baseSpeed = 5;
    [SerializeField]
    public float turnSpeed = 5;

    public float thrust = 100f;
    public Vector3 turnTorque = new Vector3(90f, 25f, 45f);
    public float forceMult = 1000f;
    public float sensitivity = 5f;

    public float speed;

    private float playerTime = 0.00f;

    private float pitch = 0f;
    private float yaw = 0f;
    private float roll = 0f;

    //Private Variabels
    private Rigidbody rigid;


    private Vector3 cameraPosition;


    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        speed = baseSpeed;
    }

    void Start()
    {
        headsetDir = ovrCamera.centerEyeAnchor.forward;
    }

    private void Update()
    {
        //var localFlyTarget = transform.InverseTransformPoint(headsetDir).normalized * sensitivity;

        Debug.DrawLine(gameObject.transform.position,ovrCamera.centerEyeAnchor.forward + Vector3.down);
        // Move player
        rigid.AddRelativeForce(ovrCamera.centerEyeAnchor.forward * speed * Time.deltaTime, ForceMode.Force);
            // rigid.position += Vector3.forward * speed * Time.deltaTime;

            // Rotate player
            rigid.AddRelativeTorque(
                new Vector3(
                    turnTorque.x * pitch,
                    turnTorque.y * yaw,
                    0f
                ) * forceMult,
                ForceMode.Force
            );
    }

}
