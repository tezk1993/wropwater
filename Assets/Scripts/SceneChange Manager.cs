﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    public int currentScene = SceneManager.GetActiveScene().buildIndex;
    public int nextScene = 0;
    void OnTriggerEnter(Collider ChangeScene)
    {
        if (ChangeScene.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadSceneAsync(currentScene+1);
        }
    }
}
