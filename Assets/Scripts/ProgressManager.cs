﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Homebrew;


[System.Serializable]
public class Ending
{
    public bool Completed = false;
    [TextArea(3, 10)]
    public string endText;

    [Header("PopUp")]
    public string achTitle;
    public string achDesc;
    [Header("Menu")]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descText;
    public bool reset;

    public AudioClip achSound;

    public RFX4_RotateAround rotationScript;
    public GameObject checkMark;

}


public class ProgressManager : MonoBehaviour
{


    public List<Ending> endings;

    //achievement UI
    [Foldout("Achievement Pop Up",true)]

    public GameObject achievementPopup;
    public bool achActive = true;
    public TextMeshProUGUI achTitle;
    public TextMeshProUGUI achDesc;

    [Foldout("Achievement Info", true)]
    public TextMeshProUGUI infoTitle;
    public GameObject infoGameObject;
    public TextMeshProUGUI infoText;


    [Foldout("Other", true)]
    public Image achievementProgress;
    public simpleMovement simpleMovementScript;
    public float currentEndings { get; set; }
    public float maxEndings { get; set; }
    public bool showingAchievement;

    private static ProgressManager playerInstance;
    public bool AchivementEnabled;
    public OVRScreenFade groundscreenFade;
    public OVRScreenFade flyingscreenFade;


    public List<arrow_Removal> arrowScripts;


    void Awake()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;


        for (int i = 0; i < endings.Count; i++)
        {
            endings[i].titleText.text = endings[i].achTitle;
            endings[i].descText.text = endings[i].achDesc;
        }

    }

    void Start()
    {
        maxEndings = endings.Count;
        achievementProgress.fillAmount = CalculateProgress();
    }


    public void toggleAchivementEnable()
    {
        if(AchivementEnabled == true)
        {
            AchivementEnabled = false;
        }
        else
        {
            AchivementEnabled = true;
        }
    }
    public void toggleShowingAchivement()
    {
        if (AchivementEnabled == true)
        {
            showingAchievement = false;
        }
        else
        {
            //In = true;
        }
    }
    /// <summary>
    /// SceneManagement
    /// </summary>
    /// <param name="index"></param>
    #region
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
       if(scene.name == "MainScene")
        {
            addMainSceneRef();
            simpleMovementScript.waterDrop.SetActive(true);
            flyingscreenFade.OnLevelFinishedLoading();
            simpleMovementScript.ResetPlayer(1);
        }
        else
        {
            groundscreenFade.OnLevelFinishedLoading();
            simpleMovementScript.waterDrop.SetActive(false);
            addMenuSceneRef();
            simpleMovementScript.ResetPlayer(0);

        }
    }

    public void reloadscene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void loadNextScene()
    {
        Debug.Log("LoadScene5");

        StartCoroutine(loadScene(1f, 1));
    }
    public void loadPreviousScene()
    {
        Debug.Log("LoadScene1");

        StartCoroutine(loadScene(1f, 0));
    }

    public void resetPlayer(int index)
    {
        removeArrows();
        simpleMovementScript.ResetPlayer(index);
    }
    IEnumerator loadScene(float delayTime,int buildIndex)
    {
        yield return new WaitForSeconds(delayTime);
        SceneManager.LoadScene(buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion

    /// <summary>
    /// Achivements
    /// </summary>
    /// <param name="index"></param>
    #region


    public void ClearMenuAchivements()
    {
        currentEndings = 0;
        achievementProgress.fillAmount = CalculateProgress();

        for (int i = 0; i < endings.Count; i++)
        {
            endings[i].checkMark.gameObject.SetActive(false);
            endings[i].rotationScript.enabled = false;
            endings[i].checkMark.gameObject.transform.parent.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
    }


    [ContextMenu("Add Achivement")]
    public void UpdateMenuAchivements(int achievementIndex)
    {
        if (AchivementEnabled)
        {
            endings[achievementIndex].checkMark.gameObject.SetActive(true);
            endings[achievementIndex].rotationScript.enabled = true;
        }
    }

  
    float CalculateProgress()
    {
        return currentEndings / maxEndings;
    }
    public bool achievementDoneCheck(int achievementIndex)
    {
        if (endings[achievementIndex].Completed)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void AchievementTrigger(int achievementIndex, EndZone triggeringEndzone)
    {
        Debug.Log("Trigger Achievement");
        if (!endings[achievementIndex].Completed)
        {
            UpdateMenuAchivements(achievementIndex);
            Debug.Log("Trigger Achievement 2");

            showAchievement(achievementIndex);
            currentEndings = currentEndings + 1;
            endings[achievementIndex].Completed = true;
            achievementProgress.fillAmount = CalculateProgress();
        }
        else
        {
            triggeringEndzone.isVisited = true;
        }

        if (endings[achievementIndex].reset)
        {
            //Debug.Log("restarting in: " + endings[achievementIndex].achSound.length + 2f.ToString());
            //Invoke("loadPreviousScene", endings[achievementIndex].achSound.length + 2f);
        }
    }

    public void showAchievement(int index)
    {
        Debug.Log("Show achievement");
        if (AchivementEnabled)
        {
            Debug.Log("Showing achievement");

            UpdateMenuAchivements(index);
            achTitle.text = endings[index].achTitle;
            achDesc.text = endings[index].achDesc;
            achievementPopup.SetActive(true);
            //Reset UI
            Invoke("DisableAchievementPopUp", 5f);
        }

        Debug.Log("play single");
        SoundManager.instance.PlaySingle(endings[index].achSound);
        infoText.text = endings[index].endText;
        infoTitle.text = endings[index].achTitle;
        infoGameObject.SetActive(true);
    }

    public void DisableAchievementPopUp()
    {
        achievementPopup.SetActive(false);
    }


    public void removeArrows()
    {
        for (int i = 0; i < arrowScripts.Count; i++)
        {
            arrowScripts[i].removeArrows();
        }
    }
    #endregion


    /// <summary>
    /// UI
    /// </summary>
    /// <param name="index"></param>
    #region
    public void addMenuSceneRef()
    {

    }

    public void addMainSceneRef()
    {

    }


    #endregion


}