﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public AudioSource fxSource;
    public AudioSource musicSource;
    public static SoundManager instance = null;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    //Used to play single sound clips.
    public void PlaySingle(AudioClip clip)
    {
        Debug.Log("play single");
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        if(clip != null)
        {
            if(clip.name == "Jump1")
            {
                fxSource.clip = clip;
                fxSource.Play();
            }
            else
            {
                fxSource.PlayOneShot(clip);
            }
        }
    }
        

    public void ChangeMusicSource(AudioClip clip)
    {
        musicSource.Stop();
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        if(clip.name == "Sewer_drain")
        {
            musicSource.volume = 0.1f;
        }
        else
        {
            musicSource.volume = 0.5f;
        }

        //Play the clip.
        musicSource.Play();
    }

}
