﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndZone : MonoBehaviour
{
    public ProgressManager progressManager;
    public bool isVisited = false;
    public int achievementIndex;



    private void Awake()
    {
        progressManager = GameObject.FindGameObjectWithTag("ProgressManager").GetComponent<ProgressManager>();
    }


    private void OnTriggerEnter(Collider finish)
    {
        //Needs to Check if this specific achievement has been achieved
        if (finish.gameObject.CompareTag("Player") ){
            progressManager.AchievementTrigger(achievementIndex,this);
        }
    }
}
