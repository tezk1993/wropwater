﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugCamera : MonoBehaviour
{
    OVRCameraRig vrCamRig;
    // Start is called before the first frame update
    void Start()
    {
        vrCamRig = gameObject.GetComponent<OVRCameraRig>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(vrCamRig.transform.position, vrCamRig.transform.forward, Color.green, 500f);
    }
}
