﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
public class LODEase : EditorWindow
{
    static string myString = "Hello World";
    bool groupEnabled;
    static bool myBool = true;
    static float moveX = 0f;
    static float moveY = 0f;
    static float moveZ = 0f;

    

    [MenuItem("Test/Make child")]
    static void newObjectToChild()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();

        SelectedGameobjects.AddRange( Selection.gameObjects);

        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            Instantiate(SelectedGameobjects[i],SelectedGameobjects[i].transform.position, SelectedGameobjects[i].transform.rotation, SelectedGameobjects[i].transform);
        }
    }

    [MenuItem("Test/Create LODS")]
    static void CreateLODS()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        Renderer[] renderers = new Renderer[3];
        
        SelectedGameobjects.AddRange(Selection.gameObjects);


        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            LOD[] lods = SelectedGameobjects[i].GetComponent<LODGroup>().GetLODs();

            for (int j = 0; j < 3; j++)
            {
                Debug.Log(i + " " + j);
                renderers[j] = SelectedGameobjects[i].transform.GetChild(0).GetComponent<Renderer>();
                Debug.Log(renderers[j].gameObject.name);
                lods[j] = new LOD(0, renderers);
            }
            SelectedGameobjects[i].GetComponent<LODGroup>().SetLODs(lods);
        }
    }

   

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/My Window")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        LODEase window = (LODEase)EditorWindow.GetWindow(typeof(LODEase));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        moveX = EditorGUILayout.FloatField("Move in X",moveX);
        moveY = EditorGUILayout.FloatField("Move in Y", moveY);
        moveZ = EditorGUILayout.FloatField("Move in Z", moveZ);
        myString = EditorGUILayout.TextField("ReName string", myString);
        myBool = EditorGUILayout.Toggle(myBool);

        if (GUI.Button(new Rect(10, 150, 100, 50), "Move Algon Axis"))
        {
            MoveALongXAxis();
        }


        if (GUI.Button(new Rect(10, 200, 100, 50), "Change Name"))
        {
            Rename();
        }

        if (GUI.Button(new Rect(10, 250, 100, 50), "Toggle LOD"))
        {
            DisableLOD();
        }
        if (GUI.Button(new Rect(10, 300, 100, 50), "CreateCubePrimitive"))
        {
            CreateCubePrimitive();
        }
        if (GUI.Button(new Rect(10, 350, 100, 50), "CreateCylinderPrimitive"))
        {
            CreateCylinderPrimitive();
        }
        if (GUI.Button(new Rect(10, 400, 100, 50), "CreatePlanePrimitive"))
        {
            CreatePlanePrimitive();
        }
    }


    private static void CreateCubePrimitive()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        List<Collider> Colliders = new List<Collider>();

        SelectedGameobjects.AddRange(Selection.gameObjects);



        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            Colliders.AddRange(SelectedGameobjects[i].GetComponents<Collider>());
            for (int j = 0; j < Colliders.Count; j++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.position = Colliders[j].bounds.center;
                cube.transform.localScale = Colliders[j].bounds.size;
                cube.name = "LOD2";
                cube.transform.SetParent(SelectedGameobjects[i].transform);
            }
     
        }


    }

    private static void CreateCylinderPrimitive()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        List<Collider> Colliders = new List<Collider>();

        SelectedGameobjects.AddRange(Selection.gameObjects);


        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            Colliders.AddRange(SelectedGameobjects[i].GetComponents<Collider>());
            for (int j = 0; j < Colliders.Count; j++)
            {
                GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                cylinder.transform.position = Colliders[j].bounds.center;
                cylinder.transform.localScale = Colliders[j].bounds.size;
                cylinder.name = "LOD2";
                cylinder.transform.SetParent(Selection.activeGameObject.transform);
            }
        }
    }

    private static void CreatePlanePrimitive()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        List<Collider> Colliders = new List<Collider>();

        SelectedGameobjects.AddRange(Selection.gameObjects);


        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            Colliders.AddRange(SelectedGameobjects[i].GetComponents<Collider>());
            for (int j = 0; j < Colliders.Count; j++)
            {
                GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);

                plane.transform.position =  Colliders[j].bounds.center;
                plane.transform.localScale = SelectedGameobjects[i].transform.localScale/2;
                plane.name = "LOD2";
                plane.transform.SetParent(SelectedGameobjects[i].transform.parent);
            }
           
        }


    }
    static void Rename()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();

        SelectedGameobjects.AddRange(Selection.gameObjects);


        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            SelectedGameobjects[i].name = myString;
        }
    }

    [MenuItem("Test/Group Selected %g")]
    private static void GroupSelected()
    {
        if (!Selection.activeTransform) return;
        var go = new GameObject(Selection.activeTransform.name + " Group");
        Undo.RegisterCreatedObjectUndo(go, "Group Selected");
        go.transform.SetParent(Selection.activeTransform.parent, false);
        foreach (var transform in Selection.transforms) Undo.SetTransformParent(transform, go.transform, "Group Selected");
        Selection.activeGameObject = go;
    }

    [MenuItem("Test/Move Along x Axis")]
    static void MoveALongXAxis()
    {
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        Renderer[] renderers = new Renderer[3];

        SelectedGameobjects.AddRange(Selection.gameObjects);


        for (int i = 0; i < SelectedGameobjects.Capacity; i++)
        {
            SelectedGameobjects[i].transform.position = new Vector3(SelectedGameobjects[i].transform.position.x + (moveX * i), SelectedGameobjects[i].transform.position.y + (moveY * i), SelectedGameobjects[i].transform.position.z + (moveZ * i));
        }
    }

    [MenuItem("Test/DisableLOD")]
    private static void DisableLOD()
    {
        if (!Selection.activeTransform) return;
        LODGroup[] LODGroups;
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        SelectedGameobjects.AddRange(Selection.gameObjects);

        

        for (int i = 0; i < SelectedGameobjects.Count; i++)
        {
            LODGroups = SelectedGameobjects[i].GetComponentsInChildren<LODGroup>();
            foreach (LODGroup lod in LODGroups)
            {
                lod.enabled = myBool;
            }
        }
       
    }

    [MenuItem("Test/UnGroup Selected %t")]
    private static void UnGroupSelected()
    {
        if (!Selection.activeTransform) return;
        List<GameObject> SelectedGameobjects = new List<GameObject>();
        List<Transform> Children = new List<Transform>();

        SelectedGameobjects.AddRange(Selection.gameObjects);

        for (int i = 0; i < SelectedGameobjects.Count; i++)
        {

            foreach (Transform child in SelectedGameobjects[i].transform)
            {
                //Debug.Log(child.name);
                Children.Add(child);
            }

            for (int j = 0; j < Children.Count; j++)
            {
                Children[j].SetParent(SelectedGameobjects[i].transform.parent);

            }
            DestroyImmediate(SelectedGameobjects[i]);
        }
     
    }
}
